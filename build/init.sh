export AWS_ACCESS_KEY_ID=$(cat /etc/secret-volume/AWS_ACCESS_KEY_ID)
export AWS_SECRET_ACCESS_KEY=$(cat /etc/secret-volume/AWS_SECRET_ACCESS_KEY)
AWS_ACCESS_KEY_ID=$(cat /etc/secret-volume/AWS_ACCESS_KEY_ID)
AWS_SECRET_ACCESS_KEY=$(cat /etc/secret-volume/AWS_SECRET_ACCESS_KEY)
echo $DB_NAME $AWS_SECRET_ACCESS_KEY $AWS_ACCESS_KEY_ID $DB_USER $S3_BUCKET_NAME $SVC_NAME
aws s3 ls
mkdir backup

pg_dump --format=plain --file=backup/$(echo $(date|sed -e 's/\s\+/-/g').sql) --dbname=$DB_NAME  -h $SVC_NAME -p 5432 -U $DB_USER  
ls backup
aws s3  sync backup s3://$S3_BUCKET_NAME
if [ $? -eq 0 ]; then
    echo DONE
else
    echo DB INVALID CONFIG
fi

