---
# Source: grafana/templates/serviceaccount.yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  labels:
    helm.sh/chart: grafana-6.50.7 
    app.kubernetes.io/name: grafana
    app.kubernetes.io/instance: my
    app.kubernetes.io/version: "9.3.6"
    app.kubernetes.io/managed-by: Helm
  name: my-grafana
  namespace: default
---
# Source: grafana/templates/secret.yaml
apiVersion: v1
kind: Secret
metadata:
  name: my-grafana
  namespace: default
  labels:
    helm.sh/chart: grafana-6.50.7
    app.kubernetes.io/name: grafana
    app.kubernetes.io/instance: my
    app.kubernetes.io/version: "9.3.6"
    app.kubernetes.io/managed-by: Helm
type: Opaque
data:
  admin-user: {{ .Values.grafana.username}}
  admin-password: {{ .Values.grafana.password}}
  ldap-toml: ""
---
# Source: grafana/templates/configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: my-grafana
  namespace: default
  labels:
    helm.sh/chart: grafana-6.50.7
    app.kubernetes.io/name: grafana
    app.kubernetes.io/instance: my
    app.kubernetes.io/version: "9.3.6"
    app.kubernetes.io/managed-by: Helm
data:
  grafana.ini: |
    [analytics]
    check_for_updates = true
    [grafana_net]
    url = https://grafana.net
    [log]
    mode = console
    [paths]
    data = /var/lib/grafana/
    logs = /var/log/grafana
    plugins = /var/lib/grafana/plugins
    provisioning = /etc/grafana/provisioning
    [server]
    domain = ''
---
# Source: grafana/templates/clusterrole.yaml
kind: ClusterRole
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  labels:
    helm.sh/chart: grafana-6.50.7
    app.kubernetes.io/name: grafana
    app.kubernetes.io/instance: my
    app.kubernetes.io/version: "9.3.6"
    app.kubernetes.io/managed-by: Helm
  name: my-grafana-clusterrole
rules: []
---
# Source: grafana/templates/clusterrolebinding.yaml
kind: ClusterRoleBinding
apiVersion: rbac.authorization.k8s.io/v1
metadata:
  name: my-grafana-clusterrolebinding
  labels:
    helm.sh/chart: grafana-6.50.7
    app.kubernetes.io/name: grafana
    app.kubernetes.io/instance: my
    app.kubernetes.io/version: "9.3.6"
    app.kubernetes.io/managed-by: Helm
subjects:
  - kind: ServiceAccount
    name: my-grafana
    namespace: default
roleRef:
  kind: ClusterRole
  name: my-grafana-clusterrole
  apiGroup: rbac.authorization.k8s.io
---
# Source: grafana/templates/role.yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  name: my-grafana
  namespace: default
  labels:
    helm.sh/chart: grafana-6.50.7
    app.kubernetes.io/name: grafana
    app.kubernetes.io/instance: my
    app.kubernetes.io/version: "9.3.6"
    app.kubernetes.io/managed-by: Helm
rules:
  - apiGroups:      ['extensions']
    resources:      ['podsecuritypolicies']
    verbs:          ['use']
    resourceNames:  [my-grafana]
---
# Source: grafana/templates/rolebinding.yaml
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  name: my-grafana
  namespace: default
  labels:
    helm.sh/chart: grafana-6.50.7
    app.kubernetes.io/name: grafana
    app.kubernetes.io/instance: my
    app.kubernetes.io/version: "9.3.6"
    app.kubernetes.io/managed-by: Helm
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: my-grafana
subjects:
- kind: ServiceAccount
  name: my-grafana
  namespace: default
---
# Source: grafana/templates/service.yaml
apiVersion: v1
kind: Service
metadata:
  name: my-grafana
  namespace: default
  labels:
    helm.sh/chart: grafana-6.50.7
    app.kubernetes.io/name: grafana
    app.kubernetes.io/instance: my
    app.kubernetes.io/version: "9.3.6"
    app.kubernetes.io/managed-by: Helm
spec:
  type: ClusterIP
  ports:
    - name: service
      port: 80
      protocol: TCP
      targetPort: 3000
  selector:
    app.kubernetes.io/name: grafana
    app.kubernetes.io/instance: my
---
# Source: grafana/templates/deployment.yaml
apiVersion: apps/v1
kind: Deployment
metadata:
  name: my-grafana
  namespace: default
  labels:
    helm.sh/chart: grafana-6.50.7
    app.kubernetes.io/name: grafana
    app.kubernetes.io/instance: my
    app.kubernetes.io/version: "9.3.6"
    app.kubernetes.io/managed-by: Helm
spec:
  replicas: 1
  revisionHistoryLimit: 10
  selector:
    matchLabels:
      app.kubernetes.io/name: grafana
      app.kubernetes.io/instance: my
  strategy:
    type: RollingUpdate
  template:
    metadata:
      labels:
        app.kubernetes.io/name: grafana
        app.kubernetes.io/instance: my
      annotations:
        checksum/config: c089c547356adaa67ee08e582665e13325eb3be981966c42e676b72a70e4bc1a
        checksum/dashboards-json-config: 01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b
        checksum/sc-dashboard-provider-config: 01ba4719c80b6fe911b091a7c05124b64eeece964e09c058ef8f9805daca546b
        checksum/secret: 700fef60fb01af813d7f23ec94fc40146732425815d3b37da76dbaf75f9bcadd
    spec:
      
      serviceAccountName: my-grafana
      automountServiceAccountToken: true
      securityContext:
        fsGroup: 472
        runAsGroup: 472
        runAsUser: 472
      enableServiceLinks: true
      containers:
        - name: grafana
          image: "grafana/grafana:9.3.6"
          imagePullPolicy: IfNotPresent
          volumeMounts:
            - name: config
              mountPath: "/etc/grafana/grafana.ini"
              subPath: grafana.ini
            - name: storage
              mountPath: "/var/lib/grafana"
          ports:
            - name: grafana
              containerPort: 3000
              protocol: TCP
            - name: gossip-tcp
              containerPort: 9094
              protocol: TCP
            - name: gossip-udp
              containerPort: 9094
              protocol: UDP
          env:
            - name: POD_IP
              valueFrom:
                fieldRef:
                  fieldPath: status.podIP
            - name: GF_SECURITY_ADMIN_USER
              valueFrom:
                secretKeyRef:
                  name: my-grafana
                  key: admin-user
            - name: GF_SECURITY_ADMIN_PASSWORD
              valueFrom:
                secretKeyRef:
                  name: my-grafana
                  key: admin-password
            - name: GF_PATHS_DATA
              value: /var/lib/grafana/
            - name: GF_PATHS_LOGS
              value: /var/log/grafana
            - name: GF_PATHS_PLUGINS
              value: /var/lib/grafana/plugins
            - name: GF_PATHS_PROVISIONING
              value: /etc/grafana/provisioning
          livenessProbe:
            failureThreshold: 10
            httpGet:
              path: /api/health
              port: 3000
            initialDelaySeconds: 60
            timeoutSeconds: 30
          readinessProbe:
            httpGet:
              path: /api/health
              port: 3000
      volumes:
        - name: config
          configMap:
            name: my-grafana
        - name: storage
          emptyDir: {}
---
# Source: grafana/templates/tests/test-serviceaccount.yaml
apiVersion: v1
kind: ServiceAccount
metadata:
  labels:
    helm.sh/chart: grafana-6.50.7
    app.kubernetes.io/name: grafana
    app.kubernetes.io/instance: my
    app.kubernetes.io/version: "9.3.6"
    app.kubernetes.io/managed-by: Helm
  name: my-grafana-test
  namespace: default
  annotations:
    "helm.sh/hook": test-success
    "helm.sh/hook-delete-policy": "before-hook-creation,hook-succeeded"
---
# Source: grafana/templates/tests/test-configmap.yaml
apiVersion: v1
kind: ConfigMap
metadata:
  name: my-grafana-test
  namespace: default
  annotations:
    "helm.sh/hook": test-success
    "helm.sh/hook-delete-policy": "before-hook-creation,hook-succeeded"
  labels:
    helm.sh/chart: grafana-6.50.7
    app.kubernetes.io/name: grafana
    app.kubernetes.io/instance: my
    app.kubernetes.io/version: "9.3.6"
    app.kubernetes.io/managed-by: Helm
data:
  run.sh: |-
    @test "Test Health" {
      url="http://my-grafana/api/health"

      code=$(wget --server-response --spider --timeout 90 --tries 10 ${url} 2>&1 | awk '/^  HTTP/{print $2}')
      [ "$code" == "200" ]
    }
---
# Source: grafana/templates/tests/test.yaml
apiVersion: v1
kind: Pod
metadata:
  name: my-grafana-test
  labels:
    helm.sh/chart: grafana-6.50.7
    app.kubernetes.io/name: grafana
    app.kubernetes.io/instance: my
    app.kubernetes.io/version: "9.3.6"
    app.kubernetes.io/managed-by: Helm
  annotations:
    "helm.sh/hook": test-success
    "helm.sh/hook-delete-policy": "before-hook-creation,hook-succeeded"
  namespace: default
spec:
  serviceAccountName: my-grafana-test
  containers:
    - name: my-test
      image: "bats/bats:v1.4.1"
      imagePullPolicy: "IfNotPresent"
      command: ["/opt/bats/bin/bats", "-t", "/tests/run.sh"]
      volumeMounts:
        - mountPath: /tests
          name: tests
          readOnly: true
  volumes:
    - name: tests
      configMap:
        name: my-grafana-test
  restartPolicy: Never
